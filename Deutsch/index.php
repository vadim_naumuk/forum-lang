<?php
$lang_index = array(
 'Topics' => 'Themen',
 'Link to' => 'Link zu:', // As in "Link to: http://fluxbb.org/"
 'Empty board' => 'Board ist leer.',
 'Newest user' => 'Letzter registrierter Benutzer: %s',
 'Users online' => 'Registrierte Benutzer online: %s',
 'Guests online' => 'Gäste online: %s',
 'No of users' => 'Gesamtzahl registrierter Benutzer: %s',
 'No of topics' => 'Gesamtzahl an Themen: %s',
 'No of posts' => 'Gesamtzahl Beiträge: %s',
 'Online' => 'Online:', // As in "Online: User A, User B etc."
 'Board info' => 'Board Information',
 'Board stats' => 'Board Statistiken',
 'User info' => 'Benutzer Iinformation'
);
?>
