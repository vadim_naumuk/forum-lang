<?php
$lang_forum = array(
 'Post topic' => 'Poste ein neues Thema',
 'Views' => 'Ansichten',
 'Moved' => 'Verschoben:',
 'Sticky' => 'Angeklebt:',
 'Closed' => 'Geschlossen:',
 'Empty forum' => 'Forum ist leer.',
 'Mod controls' => 'Moderator kontrolliert',
 'Is subscribed' => 'Du hast dieses Forum abonniert',
 'Unsubscribe' => 'Abonnement gelöscht',
 'Subscribe' => 'Abonniere dieses Forum'
);
