<?php
$lang_post = array(
 'No subject' => 'Themen müssen einen Betreff enthalten.',
 'No subject after censoring' => 'Themen müssen einen Betreff enthalten. Nach Anwendung unserer Zensurfilter ist der Betreff leer.',
 'Too long subject' => 'Ein Betreff darf nicht länger als 70 Zeichen sein.',
 'No message' => 'Du musst einen Text schreiben.',
 'No message after censoring' => 'Du musst einen Text schreiben. Nach Anwendung unserer Zensurfilter ist dein Text leer.',
 'Too long message' => 'Beiträge dürfen nicht länger als %s Bytes sein.',
 'All caps subject' => 'Ein Betreff darf nicht nur aus Großbuchstaben bestehen.',
 'All caps message' => 'Beiträge dürfen nicht nur aus Großbuchstaben bestehen.',
 'Empty after strip' => 'Scheinbar besteht dein Beitrag nur aus leerem BBCode. Dies ist möglicherweise dadurch geschehen, dass die tiefer verschachtelten Beitragsteile verworfen wurden, weil die maximal mögliche Verschachtelungstiefe überschritten wurde.',
 'Post errors' => 'Fehler im Beitrag',
 'Post errors info' => 'Die folgenden Fehler müssen korrigiert werden, bevor der Beitrag veröffentlicht werden kann:',
 'Post preview' => 'Beitragsvorschau',
 'Guest name' => 'Name', // For guests (instead of Username)
 'Post redirect' => 'Beitrag gebucht. Weiterleitung …',
 'Post a reply' => 'Verfasse eine Antwort',
 'Post new topic' => 'Verfasse ein neues Thema',
 'Hide smilies' => 'Keine Smilies als Icons in diesem Beitrag anzeigen',
 'Subscribe' => 'Abonniere dieses Thema',
 'Stay subscribed' => 'Dieses Thema weiterhin abonnieren',
 'Topic review' => 'Themen Übersicht (neuestes zuerst)',
 'Flood start' => 'Mindestens %s Sekunden müssen zwischen den Beiträgen vergehen. Bitte warte %s Sekunden ab und versuche es danach wieder.',
 'Preview' => 'Vorschau', // submit button to preview message
 'Edit post legend' => 'Ändere den Beitrag und schicke ihn mit den Änderungen ab',
 'Silent edit' => 'Verschwiegene Änderung (Keine Anzeige von "Editiert von ..." in der Beitragsansicht)',
 'Edit post' => 'Editiere Beitrag',
 'Edit redirect' => 'Beitrag aktualisiert. Weiterleitung …'
);
?>
