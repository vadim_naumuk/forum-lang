<?php
$lang_forum = array(
 'Post topic' => 'Опубликовать новую тему',
 'Views' => 'Просмотров',
 'Moved' => 'Перенесено:',
 'Sticky' => 'Стикер:',
 'Closed' => 'Закрыто:',
 'Empty forum' => 'Форум пустой.',
 'Mod controls' => 'Контролируется модератором',
 'Is subscribed' => 'Вы подписаны на данный форум',
 'Unsubscribe' => 'Отписаться',
 'Subscribe' => 'Подписаться на данный форум'
);
?>
