<?php
$lang_honeypot_sfs_plugin = array(
 'Description' => 'Данный плагин используется для контроля настроек Honeypot + StopForumSpam модулей.',
 'Options' => 'Опции',
 'Settings' => 'Настройки',
 'StopForumSpam check label' => 'StopForumSpam проверка',
 'StopForumSpam check help' => 'Если пользователь, пытающийся зарегистрироваться, не проходит проверку ловушки, проверьте IP и адрес электронной почты пользователя (не имя пользователя) из базы данных черного списка StopForumSpam. В то время как ловушка срабатывает в почти 100% правильных случаях, обслуживание StopForumSpam используется в качестве второго барьера против человеческих спаммеров.',
 'StopForumSpam API label' => 'StopForumSpam API',
 'StopForumSpam API help' => 'Ваш ключ StopForumSpam API. Если оставлено незаполненным, то о заблокированных регистрационных попытках спама не сообщат службе черного списка StopForumSpam.',
 'Options updated redirect' => 'Опции обновлены, перенаправление ...',
 'Search users head' => 'Искать пользователей',
 'Search users info' => 'Эта особенность позволяет Вам искать пользователей с URL в их подписях, но во всех постах. Это для нахождения спаммеров, которые преуспели, чтобы пройти через проверки Ловушки и StopForumSpam. Результаты поиска будут ограничены последними 50 зарегистрированными пользователями, подпадающими по эти критерии.',
 'Statistics' => 'Статистика',
 'Collecting stats since label' => 'Сбор статистики с тех пор',
 'Num days' => '%s дней',
 'Not available' => 'Недоступно',
 'Total label' => 'Всего',
 'Average last 7 days label' => 'В среднем за последние 7 дней',
 'Maximum day label' => 'Максимум дней',
 'Blocked last 14 days label' => 'Заблокировано за последние 14 дней',
 'Not spam info' => 'Не спам: %s',
 'Blocked by Honeypot info' => 'Заблокировано ловушкой: %s',
 'Blocked by SFS info' => 'Заблокировано SFS: %s',
 'per day' => 'в день',
 'Date' => 'Дата',
 'Total' => 'Всего'
);
?>
