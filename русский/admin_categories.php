<?php
$lang_admin_categories = array(
 'Must enter name message' => 'Вы должны ввести имя для категории',
 'Category added redirect' => 'Категория добавлена, перенаправление ...',
 'Category deleted redirect' => 'Категория удалена, перенаправление ...',
 'Delete category head' => 'Удалить категорию (также удалится из всех форумов и постов)',
 'Confirm delete subhead' => 'Подтвердите удаление категории',
 'Confirm delete info' => 'Вы дейстивтельно хотите удалить категорию <strong>%s</strong>?',
 'Delete category warn' => '<strong>Внимание!</strong> Удаление категории удалит её из всех форумов и постов!',
 'Must enter integer message' => 'Данная позиция должна содержать целое значение.',
 'Categories updated redirect' => 'Категории обновлены, перенаправление ...',
 'Add categories head' => 'Добавить категории',
 'Add categories subhead' => 'Добавить категории',
 'Add category label' => 'Добавить новую категорию',
 'Add new submit' => 'Добавить',
 'Add category help' => 'Имя добавляемой категории. Вы можете отредактировать имя категории позже. Перейдите %s для добавления в форум новой категории.',
 'Delete categories head' => 'Удалить категории',
 'Delete categories subhead' => 'Удалить категории',
 'Delete category label' => 'Удалить категорию',
 'Delete category help' => 'Выберите имя удаляемой категории. Вы должны подтвердить выбор категории перед удалением.',
 'Edit categories head' => 'Редактировать категории',
 'Edit categories subhead' => 'Редактировать категории',
 'Category position label' => 'Позиция',
 'Category name label' => 'Имя'
);
?>
