Asunto: Nuevo tema en el foro: '<forum_name>'

<poster> ha publicado un nuevo tema '<topic_subject>' en el foro '<forum_name>', al cual estás suscrito.

El tema se encuentra en <topic_url>

Puedes cancelar tu suscripción yendo a <unsubscribe_url>

--
<board_mailer> Remitente
(No respondas a este mensaje)
